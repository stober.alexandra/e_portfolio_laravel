
@extends('layout')
@section('heading')
<title>About LaraJobs</title>
@endsection
@section('title')
    <h1>About LaraJobs</h1>
@endsection
@section('content')


    <p class="lead">The Artisan Employment Connection</p>

    <p>LaraJobs connects talented Laravel developers with the companies who hire them.</p>

    <p>LaraJobs is built and maintained by <a href="http://userscape.com">UserScape</a>, creators of <a href="http://helpspot.com">HelpSpot</a>. UserScape founder Ian Landsman knew when he hired Laravel creator Taylor Otwell that he was joining a burgeoning community of web artisans; Laravel has since grown into the most popular PHP project on Github. Whether we’re supporting Taylor’s ongoing development of the Laravel framework, sponsoring the annual Laravel conference, Laracon, or participating in podcasts and newsletters throughout the Laravel community, the team at UserScape loves being part of the Laravel community.</p>

    <blockquote><p>“Several people a month contact me in search of Laravel or PHP developers. I just send them wandering through the Laravel community. It’s occurred to me that it’d be better if there was a specific site focused on Laravel jobs.”</p></blockquote>

    <p>LaraJobs is that site.</p>

    @endsection()

