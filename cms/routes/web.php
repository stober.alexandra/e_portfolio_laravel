<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contact', 'resourceController@contact');

Route::get('/post/{id}', 'resourceController@index');

Route::resource('test','resourceController');




Route::get('hello', function () {
    return 'Hello World!';
});

Route::get('/about', function(){
    return 'About';
});

Route::get('/hello/{name}', 'helloController@show');









Route::get('/helloWorld', function() {
    return view('helloWorld');
});

